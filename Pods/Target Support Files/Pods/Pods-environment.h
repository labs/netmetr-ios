
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// AFNetworking
#define COCOAPODS_POD_AVAILABLE_AFNetworking
#define COCOAPODS_VERSION_MAJOR_AFNetworking 1
#define COCOAPODS_VERSION_MINOR_AFNetworking 3
#define COCOAPODS_VERSION_PATCH_AFNetworking 1

// BCGenieEffect
#define COCOAPODS_POD_AVAILABLE_BCGenieEffect
#define COCOAPODS_VERSION_MAJOR_BCGenieEffect 1
#define COCOAPODS_VERSION_MINOR_BCGenieEffect 0
#define COCOAPODS_VERSION_PATCH_BCGenieEffect 0

// BlocksKit
#define COCOAPODS_POD_AVAILABLE_BlocksKit
#define COCOAPODS_VERSION_MAJOR_BlocksKit 2
#define COCOAPODS_VERSION_MINOR_BlocksKit 2
#define COCOAPODS_VERSION_PATCH_BlocksKit 5

// BlocksKit/All
#define COCOAPODS_POD_AVAILABLE_BlocksKit_All
#define COCOAPODS_VERSION_MAJOR_BlocksKit_All 2
#define COCOAPODS_VERSION_MINOR_BlocksKit_All 2
#define COCOAPODS_VERSION_PATCH_BlocksKit_All 5

// BlocksKit/Core
#define COCOAPODS_POD_AVAILABLE_BlocksKit_Core
#define COCOAPODS_VERSION_MAJOR_BlocksKit_Core 2
#define COCOAPODS_VERSION_MINOR_BlocksKit_Core 2
#define COCOAPODS_VERSION_PATCH_BlocksKit_Core 5

// BlocksKit/DynamicDelegate
#define COCOAPODS_POD_AVAILABLE_BlocksKit_DynamicDelegate
#define COCOAPODS_VERSION_MAJOR_BlocksKit_DynamicDelegate 2
#define COCOAPODS_VERSION_MINOR_BlocksKit_DynamicDelegate 2
#define COCOAPODS_VERSION_PATCH_BlocksKit_DynamicDelegate 5

// BlocksKit/MessageUI
#define COCOAPODS_POD_AVAILABLE_BlocksKit_MessageUI
#define COCOAPODS_VERSION_MAJOR_BlocksKit_MessageUI 2
#define COCOAPODS_VERSION_MINOR_BlocksKit_MessageUI 2
#define COCOAPODS_VERSION_PATCH_BlocksKit_MessageUI 5

// BlocksKit/UIKit
#define COCOAPODS_POD_AVAILABLE_BlocksKit_UIKit
#define COCOAPODS_VERSION_MAJOR_BlocksKit_UIKit 2
#define COCOAPODS_VERSION_MINOR_BlocksKit_UIKit 2
#define COCOAPODS_VERSION_PATCH_BlocksKit_UIKit 5

// CocoaAsyncSocket
#define COCOAPODS_POD_AVAILABLE_CocoaAsyncSocket
#define COCOAPODS_VERSION_MAJOR_CocoaAsyncSocket 7
#define COCOAPODS_VERSION_MINOR_CocoaAsyncSocket 3
#define COCOAPODS_VERSION_PATCH_CocoaAsyncSocket 1

// SVWebViewController
#define COCOAPODS_POD_AVAILABLE_SVWebViewController
#define COCOAPODS_VERSION_MAJOR_SVWebViewController 0
#define COCOAPODS_VERSION_MINOR_SVWebViewController 2
#define COCOAPODS_VERSION_PATCH_SVWebViewController 0

// TUSafariActivity
#define COCOAPODS_POD_AVAILABLE_TUSafariActivity
#define COCOAPODS_VERSION_MAJOR_TUSafariActivity 1
#define COCOAPODS_VERSION_MINOR_TUSafariActivity 0
#define COCOAPODS_VERSION_PATCH_TUSafariActivity 0

// libextobjc/EXTKeyPathCoding
#define COCOAPODS_POD_AVAILABLE_libextobjc_EXTKeyPathCoding
#define COCOAPODS_VERSION_MAJOR_libextobjc_EXTKeyPathCoding 0
#define COCOAPODS_VERSION_MINOR_libextobjc_EXTKeyPathCoding 2
#define COCOAPODS_VERSION_PATCH_libextobjc_EXTKeyPathCoding 5

// libextobjc/RuntimeExtensions
#define COCOAPODS_POD_AVAILABLE_libextobjc_RuntimeExtensions
#define COCOAPODS_VERSION_MAJOR_libextobjc_RuntimeExtensions 0
#define COCOAPODS_VERSION_MINOR_libextobjc_RuntimeExtensions 2
#define COCOAPODS_VERSION_PATCH_libextobjc_RuntimeExtensions 5

